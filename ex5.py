# -*- coding: utf-8 -*-
"""
Éditeur de Spyder

Ceci est un script temporaire.
"""

from utils.operations import SimpleCalculator

CALC = SimpleCalculator(10, 2)
SOMME = CALC.sum()
SOUSTRACTION = CALC.subtract()
MULTIPLICATION = CALC.multiply()
DIVISION = CALC.divide()

print(SOMME)
print(SOUSTRACTION)
print(MULTIPLICATION)
print(DIVISION)
